/**
 * @file   	OV7670.c
 * @author 	Hassan Abid
 * @version	v1.0.0
 * @date	Feb 13, 2017
 * 
 * @brief   
 */


#include "OV7670.h"
#include "i2c.h"
#include "BSP.h"
#include <string.h>

#define OV7670_I2C_ADDR         0x42
#define OV7670_hi2c             hi2c1



typedef enum{

    OV7670_IDLE = 0,
    OV7670_CAPTURE_NEXT_FRAME,
    OV7670_BUSY_CAPTURE,
    OV7670_CAPTURE_COMPLETE,
    
}OV7670_State_t;

OV7670_State_t state = OV7670_IDLE;
uint32_t frameSize = 0;



OV7670_Return_t OV7670_registerWrite(uint8_t reg, uint8_t value)
{
	if (HAL_OK == HAL_I2C_Mem_Write(&OV7670_hi2c, OV7670_I2C_ADDR, reg, I2C_MEMADD_SIZE_8BIT, &value, 1, 10))
        return OV7670_OK;
    
    return OV7670_ERROR;
}





OV7670_Return_t OV7670_registerRead(uint8_t reg, uint8_t* value)
{
//	if (HAL_OK == HAL_I2C_Mem_Read(&OV7670_hi2c, OV7670_I2C_ADDR | 0x01, reg, I2C_MEMADD_SIZE_8BIT, value, 1, 10))
//        return OV7670_OK;
    
    HAL_I2C_Master_Transmit(&OV7670_hi2c, OV7670_I2C_ADDR, &reg, 1, 10);
    HAL_Delay(20);
    HAL_I2C_Master_Receive(&OV7670_hi2c, OV7670_I2C_ADDR, value, 1, 10);
    HAL_Delay(20);
    
    return OV7670_ERROR;
    
}


OV7670_Return_t OV7670_reset()
{
    OV7670_GPIO_Reset( OV7670_RST );
    HAL_Delay(2);
    OV7670_GPIO_Set( OV7670_RST );
    HAL_Delay(2);
    
    return OV7670_OK;
}

OV7670_Return_t OV7670_FIFO_resetWrite()
{
    OV7670_GPIO_Reset( OV7670_FIFO_WR );
    HAL_Delay(5);
    OV7670_GPIO_Reset( OV7670_FIFO_WRST );
    HAL_Delay(5);
    OV7670_GPIO_Set( OV7670_FIFO_WRST );
    HAL_Delay(5);

    return OV7670_OK;
}

OV7670_Return_t OV7670_FIFO_resetRead()
{
    OV7670_GPIO_Reset( OV7670_FIFO_RRST );
    OV7670_GPIO_Reset( OV7670_FIFO_OE );
    
    HAL_Delay(1);
    OV7670_GPIO_Reset( OV7670_FIFO_RCK );
    HAL_Delay(1);
    OV7670_GPIO_Set( OV7670_FIFO_RCK );
    
    
    HAL_Delay(1);
    OV7670_GPIO_Reset( OV7670_FIFO_RCK );
    HAL_Delay(1);
    OV7670_GPIO_Set( OV7670_FIFO_RCK );
    
    HAL_Delay(1);
    OV7670_GPIO_Set( OV7670_FIFO_RRST );
    OV7670_GPIO_Reset( OV7670_FIFO_RCK );
    
    return OV7670_OK;
}

//#define OV7670_readParallelBus() \
//           (OV7670_GPIO_Read( DCMI_D0 )\
//          |(OV7670_GPIO_Read( DCMI_D1 ) << 1)\
//          |(OV7670_GPIO_Read( DCMI_D2 ) << 2)\
//          |(OV7670_GPIO_Read( DCMI_D3 ) << 3)\
//          |(OV7670_GPIO_Read( DCMI_D4 ) << 4)\
//          |(OV7670_GPIO_Read( DCMI_D5 ) << 5)\
//          |(OV7670_GPIO_Read( DCMI_D6 ) << 6)\
//          |(OV7670_GPIO_Read( DCMI_D7 ) << 7))

#define OV7670_readParallelBus()    \
        (GPIOB->IDR >> 8)


OV7670_Return_t OV7670_FIFO_read(uint8_t* data, uint32_t length)
{
    OV7670_GPIO_Reset( OV7670_FIFO_OE );
    uint8_t* iter = data;
    uint8_t* dataBufferEnd = data + (length & 0xFFFFFFFE);
    uint8_t delay = 100;
    
    
    while (iter < dataBufferEnd)
    {

        OV7670_GPIO_Set( OV7670_FIFO_RCK );
        delay = 1;
        while(delay--);
        *iter++ = (OV7670_readParallelBus());
        OV7670_GPIO_Reset( OV7670_FIFO_RCK );
 
    }
    
    if(length & 0x01)
    {
        OV7670_GPIO_Set( OV7670_FIFO_RCK );
        *iter++ = (OV7670_readParallelBus());
        OV7670_GPIO_Reset( OV7670_FIFO_RCK );
    }
    
    OV7670_GPIO_Set( OV7670_FIFO_OE );
    
    
    return OV7670_OK;
}

OV7670_Return_t OV7670_captureNextFrame()
{
    OV7670_FIFO_resetWrite();
    state = OV7670_CAPTURE_NEXT_FRAME;
    while (state != OV7670_CAPTURE_COMPLETE);
    
    OV7670_FIFO_resetRead();
    
    return OV7670_OK;
}

OV7670_Return_t OV7670_VSYNC_IRQHandler()
{
    
    if(state == OV7670_CAPTURE_NEXT_FRAME)
    {     
        OV7670_GPIO_Set( OV7670_FIFO_WR );
        state = OV7670_BUSY_CAPTURE;
    }
    else if (state == OV7670_BUSY_CAPTURE)
    {
        OV7670_GPIO_Reset( OV7670_FIFO_WR );
        state = OV7670_CAPTURE_COMPLETE;
    }
    
    return OV7670_OK;
}

OV7670_Return_t OV7670_init(char* format, uint32_t n)
{
    uint8_t pid;
    
    
    OV7670_GPIO_Reset( OV7670_PWDN );
    OV7670_reset();
    
    OV7670_registerRead(REG_PID, &pid);
    
    frameSize = n;
    
    OV7670_GPIO_Set( OV7670_FIFO_RRST );
    OV7670_GPIO_Set( OV7670_FIFO_OE );
    OV7670_GPIO_Set( OV7670_FIFO_RCK );
    OV7670_GPIO_Reset( OV7670_FIFO_WR );
    
   
//    if (OV7670_(REG_PID) != 0x76) {
//        return 0;
//    }

    OV7670_registerWrite(REG_COM7, 0x80); /* reset to default values */
    OV7670_registerWrite(REG_CLKRC, 0x80);
    OV7670_registerWrite(REG_COM11, 0x0A);
    OV7670_registerWrite(REG_TSLB, 0x04);
    OV7670_registerWrite(REG_TSLB, 0x04);
    OV7670_registerWrite(REG_COM7, 0x04); /* output format: rgb */

    OV7670_registerWrite(REG_RGB444, 0x00); /* disable RGB444 */
    OV7670_registerWrite(REG_COM15, 0xD0); /* set RGB565 */

    /* not even sure what all these do, gonna check the oscilloscope and go
     * from there... */
    OV7670_registerWrite(REG_HSTART, 0x16);
    OV7670_registerWrite(REG_HSTOP, 0x04);
    OV7670_registerWrite(REG_HREF, 0x24);
    OV7670_registerWrite(REG_VSTART, 0x02);
    OV7670_registerWrite(REG_VSTOP, 0x7a);
    OV7670_registerWrite(REG_VREF, 0x0a);
    OV7670_registerWrite(REG_COM10, 0x02);
    OV7670_registerWrite(REG_COM3, 0x04);
    OV7670_registerWrite(REG_MVFP, 0x3f);

    /* 160x120, i think */
    //OV7670_registerWrite(REG_COM14, 0x1a); // divide by 4
    //OV7670_registerWrite(0x72, 0x22); // downsample by 4
    //OV7670_registerWrite(0x73, 0xf2); // divide by 4

    /* 320x240: */
    OV7670_registerWrite(REG_COM14, 0x19);
    OV7670_registerWrite(0x72, 0x11);
    OV7670_registerWrite(0x73, 0xf1);

    // test pattern
    //OV7670_registerWrite(0x70, 0xf0);
    //OV7670_registerWrite(0x71, 0xf0);

    // COLOR SETTING
    OV7670_registerWrite(0x4f, 0x80);
    OV7670_registerWrite(0x50, 0x80);
    OV7670_registerWrite(0x51, 0x00);
    OV7670_registerWrite(0x52, 0x22);
    OV7670_registerWrite(0x53, 0x5e);
    OV7670_registerWrite(0x54, 0x80);
    OV7670_registerWrite(0x56, 0x40);
    OV7670_registerWrite(0x58, 0x9e);
    OV7670_registerWrite(0x59, 0x88);
    OV7670_registerWrite(0x5a, 0x88);
    OV7670_registerWrite(0x5b, 0x44);
    OV7670_registerWrite(0x5c, 0x67);
    OV7670_registerWrite(0x5d, 0x49);
    OV7670_registerWrite(0x5e, 0x0e);
    OV7670_registerWrite(0x69, 0x00);
    OV7670_registerWrite(0x6a, 0x40);
    OV7670_registerWrite(0x6b, 0x0a);
    OV7670_registerWrite(0x6c, 0x0a);
    OV7670_registerWrite(0x6d, 0x55);
    OV7670_registerWrite(0x6e, 0x11);
    OV7670_registerWrite(0x6f, 0x9f);

    OV7670_registerWrite(0xb0, 0x84);

    return OV7670_OK;
}

