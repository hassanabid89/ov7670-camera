/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "fatfs.h"
#include "i2c.h"
#include "sdio.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

#include "OV7670.h"
#include "BSP.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define DEBUG_huart     huart2

#define FRAME_WIDTH             ((uint32_t)240)
#define FRAME_HEIGHT            ((uint32_t)320)
#define FRAME_SIZE              (FRAME_WIDTH * FRAME_HEIGHT)
#define FRAME_BYTES_PER_PIXEL   ((uint32_t)2)
#define FRAME_BUFFER_SIZE       (FRAME_SIZE * FRAME_BYTES_PER_PIXEL)

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
uint32_t min_ui32(uint32_t a, uint32_t b )
{
    return a < b ? a : b;
}

void tprintf(const char* format, ...);
void rawToRGB(uint8_t* data, uint8_t* rgb, uint16_t length);
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
/* Fatfs object */
FATFS FatFs;
/* File object */
FIL fil;


uint8_t data[512*160];


uint32_t bytesWrittenToFile = 0;
char ppmHeader[] = "P6\n360\n240\n255\n";
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_SDIO_SD_Init();
  MX_FATFS_Init();

  /* USER CODE BEGIN 2 */
    OV7670_init("RGB", 76800);
    uint8_t pid = 0, ver = 0;
    
    OV7670_registerRead(REG_PID, &pid);
    OV7670_registerRead(REG_VER, &ver);
    
    OV7670_captureNextFrame();
    
    HAL_GPIO_ResetPin( LD5 );
    HAL_GPIO_ResetPin( LD6 );
    uint32_t bytesToRead = 0;
  	if (f_mount(&FatFs, "0:", 1) == FR_OK) {
		/* Mounted OK, turn on RED LED */
		
		
		/* Try to open file */
		if (f_open(&fil, "first.txt", FA_CREATE_ALWAYS | FA_READ | FA_WRITE) == FR_OK) {
			
			HAL_GPIO_SetPin( LD4 );
            
            for (int i = 0 ; i < FRAME_BUFFER_SIZE ; i += sizeof(data))
            {
                bytesToRead = min_ui32(FRAME_BUFFER_SIZE - i, sizeof(data));
                OV7670_FIFO_read(data, bytesToRead);            
                //rawToRGB(data, pixelBuffer, sizeof(data));
			/* If we put more than 0 characters (everything OK) */
                if (f_write(&fil, data, 
                            bytesToRead, 
                            &bytesWrittenToFile) == FR_OK && bytesWrittenToFile > 0) 
                {
                    HAL_GPIO_Toggle( LD6 );
                }
                else
                {
                    HAL_GPIO_ResetPin( LD4 );
                    HAL_GPIO_ResetPin( LD6 );
                    break;
                }
            }
			
			/* Close file, don't forget this! */
			f_close(&fil);
		}
		
		/* Unmount drive, don't forget this! */
		f_mount(0, "0:", 1);
	}
  
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    uint8_t rcvbuf[16], rcvbufpos = 0, c;
    
  while (1)
  {
        if (HAL_UART_Receive(&DEBUG_huart, &c, 1, 10000) != HAL_OK)
            continue;
      
        if ((c >= 32) && (c <= 126)) {
            rcvbuf[rcvbufpos++] = c;
        } else if (c == 13) {
            rcvbuf[rcvbufpos++] = 0;
            rcvbufpos = 0;
            if (strcmp((char *) rcvbuf, "hello") == 0) {
                tprintf("Hello to you too!\r\n");
            } else if (strcmp((char *) rcvbuf, "cap") == 0) {
                OV7670_captureNextFrame();
                tprintf("OK\r\n");
            } else if (strcmp((char *) rcvbuf, "rrst") == 0) {
                OV7670_FIFO_resetRead();
                tprintf("OK\r\n");
            } else if (strlen((char *) rcvbuf) > 5 &&
                    strncmp((char *) rcvbuf, "read ", 5) == 0) {
                        OV7670_FIFO_read(data, atoi((char*)(rcvbuf + 5)));
                        HAL_UART_Transmit(&DEBUG_huart, data, atoi((char*)(rcvbuf+5)), 10);
//                for (int i = 0; i < atoi((char *) (rcvbuf + 5)); i ++) 
//                {
//                    OV7670_FIFO_read(&c, 1);
//                    HAL_UART_Transmit(&DEBUG_huart, &c, 1, 10);
//                }
            } else if (strlen((char *) rcvbuf) > 6 &&
                    strncmp((char *) rcvbuf, "hread ", 6) == 0) {
                for (int i = 0; i < atoi((char *) (rcvbuf + 6)); i ++) {
                    OV7670_FIFO_read(&c, 1);
                    tprintf("Data: [0x%x]\r\n", c);
                }
            }
        }
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    if(GPIO_Pin == DCMI_VSYNC_Pin)
    {
        OV7670_VSYNC_IRQHandler();
    }
}
 

void rawToRGB(uint8_t* data, uint8_t* rgb, uint16_t length)
{
    uint16_t raw = 0;
    uint16_t pixelCount = 0;
    
    //Decode RGB545 format.
    for (int i = 0 ; i < length ; i+= 2)
    {
        raw = (data[i] << 8) | data[i+1];
        rgb[pixelCount++] = raw >> 11;        //red
        rgb[pixelCount++] = (raw >> 5) & 0x3F;  //blue
        rgb[pixelCount++] = raw & 0x1F;
    }

}

void tprintf(const char* format, ...)
{
    static char buff[512];
    va_list args;
    va_start(args, format);
    vsnprintf(buff,sizeof(buff) - 1, format, args);
    HAL_UART_Transmit(&DEBUG_huart, (uint8_t*)buff, strlen(buff), 1000); /// @todo: make it work with DMA and able to work with temprature sensor.
    va_end(args);
		
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
