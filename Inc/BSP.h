
#ifndef __BSP_H_
#define __BSP_H_

#include "usart.h"

#define sizeof_array( array )           (sizeof(array) / sizeof(array[0]))


#define HAL_GPIO_SetPin( name )         HAL_GPIO_WritePin( name##_GPIO_Port, name##_Pin, GPIO_PIN_SET)
#define HAL_GPIO_ResetPin( name )       HAL_GPIO_WritePin( name##_GPIO_Port, name##_Pin, GPIO_PIN_RESET)
#define HAL_GPIO_Read( name )           HAL_GPIO_ReadPin( name##_GPIO_Port, name##_Pin)
#define HAL_GPIO_Toggle( name )         HAL_GPIO_TogglePin( name##_GPIO_Port, name##_Pin )

//#define OV7670_GPIO_Set( name )         HAL_GPIO_SetPin( name )
//#define OV7670_GPIO_Reset( name )       HAL_GPIO_ResetPin( name )
#define OV7670_GPIO_Read( name )        HAL_GPIO_Read( name )


#define OV7670_GPIO_Set( name )         SET_BIT( name##_GPIO_Port->BSRR, name##_Pin)
#define OV7670_GPIO_Reset( name )       SET_BIT( name##_GPIO_Port->BSRR, (name##_Pin << 16) )
//#define OV7670_GPIO_Read( name )        HAL_IS_BIT_SET( name##_GPIO_Port->IDR, name##_Pin )



#endif
