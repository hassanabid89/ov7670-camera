/**
 * @file   	OV7670.h
 * @author 	Hassan Abid
 * @version	v1.0.0
 * @date	Feb 13, 2017
 * 
 * @brief   
 */

#ifndef OV7670_H_
#define OV7670_H_

#include <stdint.h>
#include <stdbool.h>
#include "ov7670reg.h"

typedef enum{

	OV7670_OK = 0,
	OV7670_ERROR = 1,
	OV7670_TIMEOUT	= 2,

}OV7670_Return_t;




#ifdef __cplusplus
extern "C"{
#endif 

OV7670_Return_t OV7670_FIFO_resetRead(void);
    
OV7670_Return_t OV7670_registerWrite(uint8_t reg, uint8_t value);
OV7670_Return_t OV7670_registerRead(uint8_t reg, uint8_t* value);
OV7670_Return_t OV7670_init(char* format, uint32_t n);

OV7670_Return_t OV7670_captureNextFrame(void);
OV7670_Return_t OV7670_FIFO_read(uint8_t* data, uint32_t length);

    
OV7670_Return_t OV7670_VSYNC_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif /* OV7670_H_ */
